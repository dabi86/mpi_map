.. TensorToolbox documentation master file, created by
   sphinx-quickstart on Mon Jul 22 17:52:07 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mpi_map's documentation!
=========================================

Contents:

.. toctree::
   :maxdepth: 3

.. automodule:: mpi_map
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

